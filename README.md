**Flying In-formation**

Contains the code and raw data for reproducing the results and figures from the manuscript

“Flying in-formation: A computational method for the classification of host seeking mosquito flight patterns using path segmentation and unsupervised machine learning”

MT Fowler, AJ Abbott, GPD Murray and PJ McCall 
